import React from 'react';
import { Button } from 'react-bootstrap';
import PropTypes from 'prop-types';

const Menu = ({ t, onShowDrugs: showDrugs }) => <Button variant={'secondary'} onClick={showDrugs}>{t('menu.button.drug')}</Button>;

Menu.propTypes = {
    t: PropTypes.func.isRequired,
    onShowDrugs: PropTypes.func.isRequired,
};

export default Menu;
