import { withTranslation } from 'react-i18next';
import Menu from './Menu';

export default withTranslation('common')(Menu);
