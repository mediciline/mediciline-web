import React, { Component } from 'react';
import styles from './App.module.scss'
import Header from '../components/Header';
import Menu from '../Menu';
import Drugs from '../drug/Drugs';
import PropTypes from 'prop-types';

class App extends Component {

    state = {
        showDrugs: false,
    };

    onShowDrugs = () => this.setState(prevState => ({showDrugs: !prevState.showDrugs}));

    render() {

        const { t } = this.props;
        const { showDrugs } = this.state;

        return (
            <div className={styles.app}>
                <Header title={t('welcome.title')} />
                <Menu onShowDrugs={this.onShowDrugs}/>
                {showDrugs && <Drugs/>}
            </div>
        );
    }
}

App.propTypes = {
    t: PropTypes.func.isRequired,
};

export default App;
