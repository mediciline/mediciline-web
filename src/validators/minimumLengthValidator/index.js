export const isInvalidMinimumLength = (value, minLength = 3) => value.length < minLength;
