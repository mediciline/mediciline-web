import {isInvalidFormatDate as isInvalidFormatDateFunction} from './formatDateValidator';
import {isInvalidMinimumLength as isInvalidMinimumLengthFunction} from './minimumLengthValidator';

export const isInvalidFormatDate = (date) => isInvalidFormatDateFunction(date);

export const isInvalidMinimumLength = (value) => isInvalidMinimumLengthFunction(value);
