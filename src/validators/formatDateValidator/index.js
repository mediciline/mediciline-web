const expirationDateRegex = RegExp('[0-9]{4}-[0-9]{2}');

export const isInvalidFormatDate = (date) => !(expirationDateRegex.test(date) && date.length === 7);
