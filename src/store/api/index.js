import axios from 'axios';

const baseDomain = process.env.REACT_APP_API;

export default axios.create({
    baseURL: baseDomain,
});
