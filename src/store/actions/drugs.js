import {
    ADD_DRUG_REQUEST,
    ADD_DRUG_SUCCESS,
    DELETE_DRUG_REQUEST,
    DELETE_DRUG_SUCCESS,
    FETCH_DRUGS_SUCCESS
} from '../constants/ActionTypes';

export const addDrugRequest = drug => ({
    type: ADD_DRUG_REQUEST,
    payload: {
        drug
    },
});

export const addDrugSuccess = drug => ({
    type: ADD_DRUG_SUCCESS,
    payload: {
        drug
    },
});

export const deleteDrugRequest = drugId => ({
    type: DELETE_DRUG_REQUEST,
    payload: {
        drugId
    },
});

export const deleteDrugSuccess = drugId => ({
    type: DELETE_DRUG_SUCCESS,
    payload: {
        drugId
    },
});

export const fetchDrugsSuccess = (drugs) => ({
    type: FETCH_DRUGS_SUCCESS,
    payload: {
        drugs
    }
});
