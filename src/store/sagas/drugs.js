import { put, takeEvery } from 'redux-saga/effects';
import { addDrugSuccess, deleteDrugSuccess, fetchDrugsSuccess } from '../actions/drugs';
import { ADD_DRUG_REQUEST, DELETE_DRUG_REQUEST } from '../constants/ActionTypes';
import api from '../api';

function* fetchDrugs() {
    const data = yield api.get("drugs")
            .then(response => response.data);

    yield put(fetchDrugsSuccess(data));
}

function* addDrug(action) {
    const drug = action.payload.drug;
    const data = yield api.post("drugs", drug)
        .then(response => response.data);

    yield put(addDrugSuccess(data));
}

function* deleteDrug(action) {
    const drugId = action.payload.drugId;
    yield api.delete(`drugs/${drugId}`)
        .then(response => response);

    yield put(deleteDrugSuccess(drugId));
}

export default [
    takeEvery(ADD_DRUG_REQUEST, addDrug),
    takeEvery('persist/REHYDRATE', fetchDrugs),
    takeEvery(DELETE_DRUG_REQUEST, deleteDrug),
];


