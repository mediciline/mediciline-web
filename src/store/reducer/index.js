import { combineReducers } from 'redux';
import drugs from './drugs';

export default combineReducers({
    drugs
});
