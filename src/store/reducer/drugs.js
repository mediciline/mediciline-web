import {
    ADD_DRUG_SUCCESS, DELETE_DRUG_SUCCESS, FETCH_DRUGS_SUCCESS,
} from '../constants/ActionTypes';

const initialState = [];

export default (state = initialState, action) => {
    switch (action.type) {
        case ADD_DRUG_SUCCESS: {
            return [...state, action.payload.drug];
        }
        case DELETE_DRUG_SUCCESS: {
            const drugIdToDelete = action.payload.drugId;
            return [...state.filter(drug => drug.id !== drugIdToDelete)];
        }
        case FETCH_DRUGS_SUCCESS: {
            const drugs = action.payload.drugs;
            return [...drugs];
        }
        default: {
            return state;
        }
    }
}
