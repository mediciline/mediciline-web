import PropTypes from 'prop-types';

export const drugPropTypes = PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    expirationDate: PropTypes.string.isRequired,
});
