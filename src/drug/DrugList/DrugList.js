import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { drugPropTypes } from '../propTypes/drugs';
import Table from '../../components/Table';

class DrugList extends Component {

    render() {

        const { t, drugs, onDeleteDrug: deleteDrug } = this.props;

        const headers = [
            t('table.drug.header.id'),
            t('table.drug.header.name'),
            t('table.drug.header.expiration-date')
        ];

        const data = drugs.map(o => ({
            id: o.id,
            values: [o.id, o.name, o.expirationDate],
            actions: [{
                name: t('table.drug.action.delete'),
                value: () => deleteDrug(o.id),
            }]
        }));

        return (
            <Table
                headers={headers}
                data={data}
                actionHeader={t('table.action')}
            />
        );
    }
}

DrugList.propTypes = {
    t: PropTypes.func.isRequired,
    drugs: PropTypes.arrayOf(drugPropTypes).isRequired,
    onDeleteDrug: PropTypes.func.isRequired,
};

export default DrugList;
