import { withTranslation } from 'react-i18next';
import DrugList from './DrugList';

export default withTranslation('common')(DrugList);
