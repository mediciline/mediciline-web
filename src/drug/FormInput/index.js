import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Form } from 'react-bootstrap';

class FormInput extends Component {

    render() {
        const {
            value,
            placeholder,
            label,
            errors,
            onBlur: onBlurAction,
            onChange
        } = this.props;

        const valueCapitalized = value.charAt(0).toUpperCase() + value.slice(1);
        const error = errors.join(', ');

        return (
            <Form.Group controlId={`formDrug${valueCapitalized}`}>
                <Form.Label>{label}</Form.Label>
                <Form.Control
                    type={'text'}
                    name={value}
                    placeholder={placeholder}
                    onChange={onChange}
                    onBlur={onBlurAction}
                    isInvalid={!!error}
                />
                <Form.Control.Feedback type={'invalid'}>{error}</Form.Control.Feedback>
            </Form.Group>
        )
    }
}

FormInput.defaultProps = {
    placeholder: '',
    errors: [],
};

FormInput.propTypes = {
    value: PropTypes.string.isRequired,
    placeholder: PropTypes.string,
    label: PropTypes.string.isRequired,
    errors: PropTypes.array,
    onChange: PropTypes.func.isRequired,
    onBlur: PropTypes.func.isRequired,
};

export default FormInput;
