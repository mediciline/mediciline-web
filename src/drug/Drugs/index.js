import { connect } from 'react-redux';
import Drugs from './Drugs';
import { bindActionCreators } from 'redux';
import { deleteDrugRequest } from '../../store/actions/drugs';

const mapStateToProps = state => ({
    drugs: state.drugs,
});

const mapDispatchToProps = dispatch => ({
    onDeleteDrug: bindActionCreators(
        deleteDrugRequest,
        dispatch,
    ),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Drugs);
