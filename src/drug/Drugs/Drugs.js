import React, { useState } from 'react';
import AddDrug from '../AddDrug';
import DrugList from '../DrugList';
import PopUpModal from '../../components/PopUpModal';
import PropTypes from 'prop-types';
import { drugPropTypes } from '../propTypes/drugs';

const Drugs = ({drugs, onDeleteDrug}) => {

    const [showConfirmDeleteDrugModal, setShowConfirmDeleteDrugModal] = useState(false);
    const [drugIdToDelete, setDrugIdToDelete] = useState(-1);

    const onShowConfirmDeleteDrugModal = (drugId) => {
        setShowConfirmDeleteDrugModal(true);
        setDrugIdToDelete(drugId);
    };

    const closeConfirmDeleteDrugModal = () => {
        setShowConfirmDeleteDrugModal(false);
    };

    const deleteDrug = () => {
        setShowConfirmDeleteDrugModal(false);
        onDeleteDrug(drugIdToDelete);
        setDrugIdToDelete(-1);
    };

    return (
        <>
            <AddDrug/>
            <DrugList drugs={drugs} onDeleteDrug={onShowConfirmDeleteDrugModal} />
            {showConfirmDeleteDrugModal && <PopUpModal
                title={'modal.drug.delete.title'}
                onClose={closeConfirmDeleteDrugModal}
                onConfirm={deleteDrug}
            />}
        </>
    );
};

Drugs.propTypes = {
    drugs: PropTypes.arrayOf(drugPropTypes),
    onDeleteDrug: PropTypes.func.isRequired,
};

export default Drugs;
