import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button, Form, Container } from 'react-bootstrap';
import FormInput from '../FormInput';
import { isInvalidFormatDate, isInvalidMinimumLength } from '../../validators';

class AddDrug extends Component {

    state = {
        drugFields: [
            {
                field: 'name',
                value: '',
                validators: [
                    {
                        validate: isInvalidMinimumLength,
                        errorMessage: 'validator.drug.minimum-three'
                    }
                ],
            },
            {
                field: 'expirationDate',
                value: '',
                validators: [
                    {
                        validate: isInvalidFormatDate,
                        errorMessage: 'validator.drug.invalid-expiration-date'
                    }
                ],
            },
        ],
        errors: [],
    };

    changeForm = ({target: {value, name}}) => {
        const { drugFields } = this.state;

        const indexChangedObject = drugFields.findIndex(o => o.field === name);

        this.setState(prevState => {
            const currentDrugFields = [...prevState.drugFields];
            currentDrugFields[indexChangedObject] = {
                ...currentDrugFields[indexChangedObject],
                field: name,
                value: value,
            };
            return {
                ...prevState,
                drugFields: currentDrugFields
            };
        });
    };

    findErrors = (value, name) => {
        const { drugFields } = this.state;
        const { t } = this.props;

        const validatedObject = drugFields.find(drugField => drugField.field === name);

        if (validatedObject === undefined) return;

        const foundedErrors = validatedObject.validators
            .filter(validator => validator.validate(value) === true)
            .map(validator => t(validator.errorMessage));

        return {
            field: name,
            values: foundedErrors
        }
    };

    validate = ({target: {value, name}}) => {
        const foundedErrors = this.findErrors(value, name);

        if (foundedErrors === undefined) return;

        this.setState(prevState => {
            const currentErrors = [...prevState.errors];
            const fieldIndex = currentErrors.findIndex(obj => obj.field === name);

            if (fieldIndex === -1) {
                currentErrors.push(foundedErrors)
            } else if (foundedErrors.values.length === 0) {
                currentErrors.splice(fieldIndex, 1)
            } else {
                currentErrors[fieldIndex] = foundedErrors;
            }
            return {
                ...prevState,
                errors: currentErrors,
            }
        });
    };

    addDrug = () => {
        const { onAddDrug } = this.props;
        const { drugFields } = this.state;

        const errors = drugFields
            .map(drugField => this.findErrors(drugField.value, drugField.field))
            .filter(error => error.values.length > 0)
            .flat();

        if (errors.length === 0) {
            const drugToSave = {};
            drugFields.forEach(o => drugToSave[o.field] = o.value);
            onAddDrug(drugToSave);
        } else {
            this.setState({
                errors: [...errors]
            })
        }
    };

    getErrorsByField = (field) => {
        const { errors } = this.state;

        return errors.filter(error => error.field === field)
            .map(error => error.values);
    };

    render() {
        const { t } = this.props;

        return (
            <Container>
                <Form>
                    <FormInput
                        value={'name'}
                        placeholder={t('form.drug.name-placeholder')}
                        label={t('form.drug.name-label')}
                        onChange={this.changeForm}
                        errors={this.getErrorsByField('name')}
                        onBlur={this.validate}
                    />
                    <FormInput
                        value={'expirationDate'}
                        placeholder={t('form.drug.expiration-date-placeholder')}
                        label={t('form.drug.expiration-date-label')}
                        onChange={this.changeForm}
                        errors={this.getErrorsByField('expirationDate')}
                        onBlur={this.validate}
                    />
                    <Button variant={'secondary'} type={'button'} onClick={this.addDrug}>
                        {t('form.drug.add-drug-button')}
                    </Button>
                </Form>
            </Container>
        )
    }
}

AddDrug.propTypes = {
    t: PropTypes.func.isRequired,
    onAddDrug: PropTypes.func.isRequired,
};

export default AddDrug;
