import { withTranslation } from 'react-i18next';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import AddDrug from './AddDrug';
import { addDrugRequest } from '../../store/actions/drugs';

const mapDispatchToProps = dispatch => ({
    onAddDrug: bindActionCreators(
        addDrugRequest,
        dispatch,
    ),
});

export default withTranslation('common')(connect(
    null,
    mapDispatchToProps,
)(AddDrug));
