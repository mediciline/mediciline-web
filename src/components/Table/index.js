import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Table as TableBootstrap } from 'react-bootstrap';
import Row from './Row';
import { rowDataPropTypes } from './propTypes/row';

class Table extends Component {

    render() {
        const { headers, actionHeader, data } = this.props;

        return (
            <TableBootstrap striped bordered hover>
            <thead>
                <tr>
                    {headers.map((header, index) => <th key={index}>{header}</th>)}
                    {actionHeader && <th>{actionHeader}</th>}
                </tr>
            </thead>
            <tbody>
                {data.map(o => <tr key={o.id}><Row data={o}/></tr>)}
            </tbody>
        </TableBootstrap>
        )
    }
}

Table.defaultProps = {
    data: [],
    headers: [],
    actionHeader: '',
};

Table.propTypes = {
    data: PropTypes.arrayOf(rowDataPropTypes),
    headers: PropTypes.arrayOf(PropTypes.string.isRequired),
    actionHeader: PropTypes.string,
};

export default Table;
