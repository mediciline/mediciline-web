import React from 'react';
import { Button } from 'react-bootstrap';
import { rowDataPropTypes } from '../propTypes/row';

const Row =  ({ data: {values, actions = []} }) => {

    const generateValues = () => values && values.map((o, index) => <td key={index}>{o}</td>);
    const generateActions = () => {
        return <td>
            {actions.map((o, index) =>
                <Button key={index} variant="secondary" onClick={o.value}>{o.name}</Button>
            )}
        </td>
    };

    return (
        <>
        {values && generateValues()}
        {actions.length > 0 && generateActions()}
    </>
    )
};

Row.propTypes = {
    data: rowDataPropTypes
};

export default Row;
