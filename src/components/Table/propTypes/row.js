import PropTypes from 'prop-types';

export const rowDataPropTypes = PropTypes.shape({
    id: PropTypes.string.isRequired,
    values: PropTypes.array.isRequired,
    actions: PropTypes.arrayOf(PropTypes.shape({
        name: PropTypes.string.isRequired,
        value: PropTypes.func.isRequired,
    })),
});
