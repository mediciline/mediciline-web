import { withTranslation } from 'react-i18next';
import PopUpModal from './PopUpModal';

export default withTranslation('common')(PopUpModal);
