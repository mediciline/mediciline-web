import React from 'react';
import PropTypes from 'prop-types';
import { Modal, Button } from 'react-bootstrap';

const PopUpModal = ({
                        t,
                        title,
                        body,
                        onClose: close,
                        onConfirm: confirm,
                        closeButtonText,
                        confirmButtonText,
                    }) => {

    return (
        <Modal show={true} onHide={close}>
            <Modal.Header>
                <Modal.Title>{t(title)}</Modal.Title>
            </Modal.Header>
            <Modal.Body>{t(body)}</Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={close}>
                    {t(closeButtonText)}
                </Button>
                <Button variant="primary" onClick={confirm}>
                    {t(confirmButtonText)}
                </Button>
            </Modal.Footer>
        </Modal>
    )
};

PopUpModal.defaultProps = {
    body: '',
    closeButtonText: 'modal.default.button.close',
    confirmButtonText: 'modal.default.button.confirm',
};

PopUpModal.propTypes = {
    t: PropTypes.func.isRequired,
    title: PropTypes.string.isRequired,
    body: PropTypes.string,
    onClose: PropTypes.func.isRequired,
    onConfirm: PropTypes.func.isRequired,
    closeButtonText: PropTypes.string,
    confirmButtonText: PropTypes.string,
};

export default PopUpModal;
